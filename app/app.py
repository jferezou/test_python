"""Main application module."""
import logging.config
import os
from pprint import pformat

import yaml
from click import echo
from flask_admin.contrib.sqla import ModelView
from flask_log_request_id import RequestID
from flask_praetorian import PraetorianError
from werkzeug.middleware.proxy_fix import ProxyFix

from app.flask_extended import Flask
from app.models.user_model import UserModel

from .api import exposed_api
from .extensions import admin, db, guard, migrate
from .models.restaurant_model import RestaurantModel


def create_app(testing=False):
    """Application factory, used to create application."""
    app = Flask("test_peopleDoc")
    app.wsgi_app = ProxyFix(app.wsgi_app)
    configure_app(app, testing)
    setup_logging(app)
    configure_extensions(app, testing)
    register_blueprints(app)

    return app


def configure_app(app, testing):
    """Set configuration for application."""
    app.config.load_config_from_yaml(testing)

    if app.config["SHOW_CONFIG"] and app.debug:
        echo(pformat(app.config))


def setup_logging(
    app, default_path="app/logging.yml", default_level=logging.INFO, env_key="LOG_CONF"
):
    """Setup logging configuration."""
    path = default_path
    value = app.config.get(env_key, None)
    if value:
        path = value

    echo(" * Logger using config from: {}".format(path))

    if not os.path.isabs(path):
        path = os.path.join(app.root_path, path)

    if os.path.exists(path):
        with open(path, "rt") as f:
            config_yaml = yaml.safe_load(f.read())
        logging.config.dictConfig(config_yaml)
    else:
        logging.basicConfig(level=default_level)

    RequestID(app)


def configure_extensions(app, testing=False):
    """Configure flask extensions."""
    db.init_app(app)
    guard.init_app(app, UserModel)
    PraetorianError.register_error_handler_with_flask_restplus(exposed_api.api)
    migrate.init_app(app, db)

    if not testing:
        admin.init_app(app)
        admin.add_view(ModelView(RestaurantModel, db.session))


def register_blueprints(app):
    """Register all blueprints for application."""
    app.register_blueprint(exposed_api.blueprint)
