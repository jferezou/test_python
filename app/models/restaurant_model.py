"""User model."""

from app.extensions import db
from app.models.db_common_model import DbCommonModel


class RestaurantModel(DbCommonModel):
    """
    Restaurant entity
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=True, nullable=False)

    @classmethod
    def get_ids(cls):
        """
        Return random restaurant from database
        :return:
        """
        return cls.query.with_entities(cls.id).all()

    @classmethod
    def get_by_name(cls, restaurant_name):
        """Get all instances of model verifying kwargs filter.
        :param restaurant_name:
        :return:
        """
        return cls.query.filter_by(name=restaurant_name).one_or_none()

    def __repr__(self):
        return "Restaurant : {} - {}".format(self.id, self.name)

    def __eq__(self, other):
        if not isinstance(other, RestaurantModel):
            return NotImplemented
        return self.id == other.id and self.name == other.name

    def __hash__(self):
        return hash((self.id, self.name))
