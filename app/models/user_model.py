"""User model."""

from app.extensions import db
from app.models.db_common_model import DbCommonModel


class UserModel(DbCommonModel):
    """User, used for authentification."""

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(100), unique=False, nullable=False)
    roles = db.Column(db.String(100), unique=False, nullable=False, default="user")

    @property
    def rolenames(self):
        """Enumerate roles for user."""
        try:
            return self.roles.split(",")
        except Exception:
            return []

    @classmethod
    def lookup(cls, uname):
        """Get the first user with given username."""
        return cls.query.filter_by(username=uname).first()

    @property
    def identity(self):
        """Get unique indentifier for user."""
        return self.id
