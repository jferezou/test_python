from app.extensions import db


class DbCommonModel(db.Model):
    """Abstract Model class adding DAO operations."""

    __abstract__ = True

    @classmethod
    def identify(cls, id_to_search):
        """Fetch an instance given its id.
        :param id_to_search: the id
        :return:
        """
        return cls.query.get_or_404(id_to_search)

    @classmethod
    def all(cls):
        """Get all instances of model."""
        return cls.query.all()
