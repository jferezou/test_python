import logging
import random
from typing import List

from app.extensions import db
from app.models import restaurant_model
from app.models.restaurant_model import RestaurantModel

logger = logging.getLogger(__name__)


def get_random_restaurant() -> RestaurantModel:
    """
    Return random restaurant
    :return:
    """
    logger.debug("Get random restaurant")
    restaurant_ids_list = RestaurantModel.get_ids()
    if not restaurant_ids_list:
        raise ValueError("No restaurant found")
    random_restaurant_id = random.choice(restaurant_ids_list)
    random_restaurant = RestaurantModel.identify(random_restaurant_id[0])
    logger.debug("Random restaurant found : {}".format(restaurant_model))
    return random_restaurant


def add_restaurant(restaurant_name: str) -> RestaurantModel:
    """
    Add a new restaurant
    :return:
    """
    logger.debug("Add a new restaurant : {}".format(restaurant_name))
    restaurant = RestaurantModel(name=restaurant_name)
    db.session.add(restaurant)
    db.session.commit()
    logger.debug("Restaurant added")
    return restaurant


def get_all_restaurants() -> List[RestaurantModel]:
    """
    Get all restaurant
    :return:
    """
    all_restaurant = RestaurantModel.all()
    logger.debug("All restaurant found : {}".format(all_restaurant))
    return all_restaurant


def delete_restaurants(restaurant_name: str) -> List[int]:
    """
    Delete specific restaurants if it exist
    :param restaurant_name:
    :param name:
    :return:
    """
    logger.debug("Delete restaurants named : {}".format(restaurant_name))
    restaurants_to_delete = RestaurantModel.get_by_name(restaurant_name)
    if not restaurants_to_delete:
        raise ValueError("Restaurant {} does not exist".format(restaurant_name))
    db.session.delete(restaurants_to_delete)
    db.session.commit()
    logger.debug("Deleted restaurant : {}".format(restaurants_to_delete.id))
    return restaurants_to_delete
