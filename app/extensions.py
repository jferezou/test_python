"""
Extensions registry.

All extensions here are used as singletons and
initialized in application factory
"""
from flask_admin import Admin
from flask_migrate import Migrate
from flask_praetorian import Praetorian
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
guard = Praetorian()
migrate = Migrate()
admin = Admin(name="Test PeopleDoc")
