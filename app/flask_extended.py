import os

import yaml
from flask import Config as BaseConfig
from flask import Flask as BaseFlask

DEFAULT_CONFIG_FILE = "config/production/config.yml"
TESTING_CONFIG_FILE = "config/testing/config.yml"


class Config(BaseConfig):
    """Flask config enhanced with a `from_yaml` method."""

    def load_config_from_yaml(self, testing=False):

        if testing:
            config_file = TESTING_CONFIG_FILE
        else:
            config_file = os.environ.get("CONFIG_FILE", DEFAULT_CONFIG_FILE)
        with open(config_file) as f:
            properties_list = yaml.load(f, Loader=yaml.FullLoader)
        for key, value in properties_list.items():
            self[key] = value
            os.environ[key] = str(value)


class Flask(BaseFlask):
    """Extended version of `Flask` that implements custom config class"""

    def make_config(self, instance_relative=False):
        root_path = self.root_path
        if instance_relative:
            root_path = self.instance_path
        return Config(root_path, self.default_config)
