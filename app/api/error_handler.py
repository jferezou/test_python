import logging
from http import HTTPStatus

from flask_log_request_id import request_id

from app.api.exposed_api import api

logger = logging.getLogger(__name__)


@api.errorhandler(Exception)
def handle_internal_error(error):
    """
    Interceptor for http errors
    :param error:
    :return:
    """
    logger.exception(error)
    # error.descripton is used for http werkzeug exception
    msg = (
        error.description
        if hasattr(error, "description") and error.description is not None
        else str(error)
    )
    http_status_code = (
        error.code
        if hasattr(error, "code") and error.code
        else error.status_code
        if hasattr(error, "status_code") and error.status_code
        else HTTPStatus.INTERNAL_SERVER_ERROR
    )

    body = {
        "type": error.__class__.__name__,
        "message": msg,
        "requestId": request_id.current_request_id(),
    }
    # restplus send data always in body, we get informations here
    error.data = body
    if hasattr(error, "error_code") and error.error_code is not None:
        body["error_code"] = error.error_code

    if hasattr(error, "detail") and error.detail is not None:
        body["detail"] = error.detail

    return body, http_status_code
