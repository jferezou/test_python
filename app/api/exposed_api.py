"""Exposed API module."""
from flask import Blueprint
from flask_restplus import Api

from app.api.namespaces.common.authentification import \
    api as ns_authentification
from app.api.namespaces.common.restaurant import api as ns_restaurant
from app.version import API_VERSION

blueprint = Blueprint("api", __name__, url_prefix="/api/v" + API_VERSION)

api = Api(
    blueprint,
    authorizations={
        "Bearer": {"type": "apiKey", "in": "header", "name": "Authorization"}
    },
    doc="/swagger",
)
api.add_namespace(ns_authentification)
api.add_namespace(ns_restaurant)
