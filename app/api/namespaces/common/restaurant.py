import logging

import flask_praetorian
from flask_restplus import Namespace, Resource, fields

from app.api.namespaces.common import ADMIN_ROLE, DELETE_ROLE, POST_ROLE
from app.service import restaurant_service

api = Namespace(
    "restaurants", description="Restaurants operations", path="/restaurants"
)

restaurant_schema = api.model("Restaurant", {"name": fields.String(required=True)})
return_restaurant_schema = api.model(
    "Return restaurant value",
    {"id": fields.Integer(required=True), "name": fields.String(required=True)},
)
logger = logging.getLogger(__name__)


@api.route("/")
class RestaurantRessource(Resource):
    """
    Ressource to handle restaurant endpoints
    """

    @flask_praetorian.roles_accepted(ADMIN_ROLE, POST_ROLE)
    @api.expect(restaurant_schema)
    @api.marshal_with(return_restaurant_schema)
    def post(self):
        input_value = api.payload
        restaurant = restaurant_service.add_restaurant(input_value["name"])
        return restaurant

    @api.marshal_list_with(return_restaurant_schema)
    def get(self):
        return restaurant_service.get_all_restaurants()


@api.route("/<name>")
class RestaurantDeleteRessource(Resource):
    """
    Ressource to handle delete a restaurant by name
    """

    @flask_praetorian.roles_accepted(ADMIN_ROLE, DELETE_ROLE)
    @api.marshal_list_with(return_restaurant_schema)
    def delete(self, name):
        return restaurant_service.delete_restaurants(name)


@api.route("/random")
class RestaurantRandomRessource(Resource):
    """
    Ressource to handle restaurant random endpoints
    """

    @api.marshal_with(return_restaurant_schema)
    def get(self):
        return restaurant_service.get_random_restaurant()
