import logging

import flask
from flask_restplus import Namespace, Resource, fields

from app.extensions import guard

api = Namespace(
    "authentification", description="Authentification operations", path="/auth"
)

login_schema = api.model(
    "Login",
    {
        "username": fields.String(required=True),
        "password": fields.String(required=True),
    },
)
logger = logging.getLogger(__name__)


@api.route("/login")
class LoginResource(Resource):
    """Pseudo Resource to handle login."""

    @api.expect(login_schema)
    def post(self):
        """Log a user with his username and password, return a JWT Token."""
        username = api.payload.get("username", None)
        password = api.payload.get("password", None)
        user = guard.authenticate(username, password)
        res = {
            "access_token": guard.encode_jwt_token(
                user,
                override_access_lifespan=None,
                override_refresh_lifespan=None,
                username=user.username,
            )
        }
        return res


@api.route("/refresh")
class RefreshResource(Resource):
    """Pseudo Resource to handle token refresh."""

    def get(self):
        """
        Refresh an existing JWTself.

        It creates a new one that is a copy of the old
        except that it has a refrehsed access expiration.
        .. example::
            $ curl http://localhost:5000/refresh -X GET \
                -H "Authorization: Bearer <your_token>"
        """
        print(flask.request.headers)
        old_token = guard.read_token_from_header()
        new_token = guard.refresh_jwt_token(old_token)
        logger.debug("\nold token = {}\nnew token = {}".format(old_token, new_token))
        ret = {"access_token": new_token}
        return ret, 200
