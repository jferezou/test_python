# technical stack
python 3.6\
flask\
SQLalchemy\
alembic\
praterorian\
postgres 9.6

# Project init
### Database init
create database and user :
-   create database test_people_doc;
-   CREATE USER test_people_doc_usr WITH PASSWORD 'test_people_doc_usr_pwd';
-   GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO test_people_doc_usr;
-   grant all privileges on all sequences in schema public to test_people_doc_usr;


### Configuration

copy and edit configuration file (example : config/development/config.yml):
-   set database url
-   set logging file path (example : config/logging.yml)

set config file path in .env file

# run application
pipenv shell\
pipenv install\
pipenv run flask db upgrade (database init scripts)\
pipenv run flask run

-   default user with all rights: admin/admin
-   default user with delete rights: delete/admin
-   default user with post rights: post/admin

you need to get a valid token to access to POST and DELETE url :
-   curl -X POST "http://127.0.0.1:5000/api/v1/auth/login" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \\"username\\": \\"**username**\\",  \\"password\\": \\"**password**\\"}"

get the response token and access endpoints with :\
-   curl -X POST "http://127.0.0.1:5000/api/v1/restaurants/" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \\"name\\": \\"restaurant 01\\"}" -H "Authorization: Bearer **validToken**}"

-   curl -X DELETE "http://127.0.0.1:5000/api/v1/restaurants/k" -H  "accept: application/json" -H "Authorization: Bearer **validToken**"

random :
- curl -X GET "http://127.0.0.1:5000/api/v1/restaurants/random" -H  "accept: application/json"

get all :
-   curl -X GET "http://127.0.0.1:5000/api/v1/restaurants/" -H  "accept: application/json"

### swagger documentation
http://127.0.0.1:5000/api/v1/swagger


logging :
unique request id for log parsing (ELK ...)


# improvements
-   finish api unit test for get, random and delete endpoint
-   finish model unit test
-   finish service unit test
-   some exception are not correctly catch by error handler (IntegrityError ...), investigate this issue
