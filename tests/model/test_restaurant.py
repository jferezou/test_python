import pytest
from assertpy import assert_that
from werkzeug.exceptions import NotFound

from app.models.restaurant_model import RestaurantModel
from tests.model.dao_test_utils import run_sql_script

pytestmark = [pytest.mark.unit]


class TestRestaurantModelIdentify:
    def test_identify_ok(self, session):
        """ Test the identification of a restaurant in BDD """

        # insertion of data test
        run_sql_script(session, "tests/model/insert_restaurants.sql")

        # try to get the object
        restaurant = RestaurantModel.identify(2)
        assert_that(restaurant.id).is_equal_to(2)
        assert_that(restaurant.name).is_equal_to("restaurant 02")

        restaurant = RestaurantModel.identify(156)
        assert_that(restaurant.id).is_equal_to(156)
        assert_that(restaurant.name).is_equal_to("restaurant test")

    def test_identify_ko(self, session):
        """ Test the identification of a restaurant in BDD """

        # insertion of data test
        run_sql_script(session, "tests/model/insert_restaurants.sql")

        # try to get the object
        with pytest.raises(NotFound):
            RestaurantModel.identify(25)


class TestRestaurantModelGetByName:
    def test_get_by_name_ok(self, session):
        """ Test the identification of a restaurant by name in BDD """

        # insertion of data test
        run_sql_script(session, "tests/model/insert_restaurants.sql")

        # try to get the object
        restaurant = RestaurantModel.get_by_name("restaurant 04")

        assert_that(restaurant.id).is_equal_to(4)
        assert_that(restaurant.name).is_equal_to("restaurant 04")

    def test_get_by_name_none(self, session):
        """ Test the identification of a restaurant by name in BDD """

        # insertion of data test
        run_sql_script(session, "tests/model/insert_restaurants.sql")

        # try to get the object
        restaurant = RestaurantModel.get_by_name("restaurant 45")

        assert_that(restaurant).is_none()


class TestRestaurantModelGetIds:
    def test_get_ids(self, session):
        """ Test the return of all restaurant ids """

        # insertion of data test
        run_sql_script(session, "tests/model/insert_restaurants.sql")

        random_restaurant_ids_list = RestaurantModel.get_ids()
        assert_that(random_restaurant_ids_list).is_equal_to(
            [(1,), (2,), (3,), (4,), (5,), (6,), (156,)]
        )

    def test_get_ids_empty(self, session):
        """ Test the return of all restaurant ids """
        random_restaurant_ids_lis = RestaurantModel.get_ids()
        assert_that(random_restaurant_ids_lis).is_empty()
