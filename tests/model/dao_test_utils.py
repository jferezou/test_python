def run_sql_script(session, file_path):
    # Open the .sql file
    sql_file = open(file_path, "r")
    # Create an empty command string
    # Iterate over all lines in the sql file
    for line in sql_file:
        sql_command = ""
        # Ignore commented lines
        if not line.startswith("--") and line.strip("\n"):
            # Append line to the command string
            sql_command += line.strip("\n")
            # If the command string ends with ';', it is a full statement
            if sql_command.endswith(";"):
                session.execute(sql_command)
