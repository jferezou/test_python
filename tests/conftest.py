import pytest

from app.app import create_app
from app.extensions import db as _db


@pytest.fixture()
def test_client():
    flask_app = create_app(testing=True)

    # Flask provides a way to test your application by exposing the Werkzeug test Client
    # and handling the context locals for you.
    testing_client = flask_app.test_client()
    # Establish an application context before running the tests.
    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client  # this is where the testing happens!

    ctx.pop()


@pytest.fixture(scope="module")
def app():
    app = create_app(testing=True)
    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    yield app

    ctx.pop()


@pytest.fixture
def db(app):
    """Session-wide test database."""
    _db.create_all()

    yield _db

    _db.drop_all()


@pytest.fixture
def session(db):
    """Creates a new database session for a test."""
    session = db.create_scoped_session()
    db.session = session

    yield session

    session.close()
    session.remove()
