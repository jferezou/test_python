from unittest.mock import Mock, patch

import pytest
from assertpy import assert_that

from app.extensions import db
from app.models.restaurant_model import RestaurantModel
from app.service import restaurant_service

pytestmark = [pytest.mark.unit]


class TestRandomRestaurant:
    @patch.object(RestaurantModel, "get_ids", return_value=[(3,)])
    @patch.object(
        RestaurantModel, "identify", return_value=RestaurantModel(id=56, name="test")
    )
    def test_get_random_restaurant(self, mock_identify, mock_get_ids):
        random_value = restaurant_service.get_random_restaurant()
        mock_get_ids.assert_called_once()
        mock_identify.assert_called_once_with(3)
        assert_that(random_value.id).is_equal_to(56)
        assert_that(random_value.name).is_equal_to("test")

    @patch.object(RestaurantModel, "get_ids", return_value=[])
    @patch.object(
        RestaurantModel, "identify", return_value=RestaurantModel(id=56, name="test")
    )
    def test_get_random_restaurant_empty(self, mock_identify, mock_get_ids):
        with pytest.raises(ValueError):
            restaurant_service.get_random_restaurant()
        mock_get_ids.assert_called_once()
        mock_identify.assert_not_called()


class TestAddRestaurant:
    def test_add_restaurant(self):
        session_mock = db.session = Mock()
        new_restaurant = restaurant_service.add_restaurant("new restaurant")
        session_mock.add.assert_called_once_with(RestaurantModel(name="new restaurant"))
        session_mock.commit.assert_called_once()
        assert_that(new_restaurant.name).is_equal_to("new restaurant")
