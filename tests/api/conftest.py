import json

import pytest

from app.app import create_app
from app.extensions import guard
from app.models.user_model import UserModel


@pytest.fixture
def app():
    """
    Configuration app particulière aux API car on ne veut pas préremplir le jwt_data
    :return:
    """
    app = create_app(testing=True)
    return app


@pytest.fixture
def admin_user(session):
    user = UserModel(
        username="admin", password=guard.encrypt_password("admin"), roles="admin_role"
    )

    session.add(user)
    session.commit()

    return user


@pytest.fixture
def admin_headers(admin_user, client):
    data = {"username": admin_user.username, "password": "admin"}
    rep = client.post(
        "/api/v1/auth/login",
        data=json.dumps(data),
        headers={"content-type": "application/json"},
    )

    tokens = json.loads(rep.get_data(as_text=True))
    return {
        "content-type": "application/json",
        "authorization": "Bearer %s" % tokens["access_token"],
    }
