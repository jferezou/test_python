from unittest.mock import patch

import pytest

from app.service import restaurant_service
from tests.utils_test import authorization_header_valid

pytestmark = [pytest.mark.api, pytest.mark.integration]


class TestPostMethod:
    path = "/api/v1/restaurants/"

    @patch.object(restaurant_service, "add_restaurant", return_value="my_return_value")
    def test_create_ok_post_role(self, add_restaurants_mock, test_client):
        data = {"name": "new_restaurant"}
        header = authorization_header_valid(roles="post_role")
        test_client.post(self.path, headers=header, json=data)
        add_restaurants_mock.assert_called_once_with("new_restaurant")

    @patch.object(restaurant_service, "add_restaurant", return_value="my_return_value")
    def test_create_ok_admin_role(self, add_restaurants_mock, test_client):
        data = {"name": "new_restaurant"}
        header = authorization_header_valid(roles="admin_role")
        test_client.post(self.path, headers=header, json=data)
        add_restaurants_mock.assert_called_once_with("new_restaurant")

    @patch.object(restaurant_service, "add_restaurant", return_value="my_return_value")
    def test_create_ko_wrong_role(self, add_restaurants_mock, test_client):
        data = {"name": "new_restaurant"}
        header = authorization_header_valid(roles="wrong_role")
        test_client.post(self.path, headers=header, json=data)
        add_restaurants_mock.assert_not_called()
