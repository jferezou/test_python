from app.extensions import guard
from app.models.user_model import UserModel


def get_valid_token(roles, username=None, password=None):
    user = UserModel(roles=roles, username=username, password=password)
    valid_token = guard.encode_jwt_token(
        user,
        override_access_lifespan=None,
        override_refresh_lifespan=None,
        username=user.username,
    )
    return valid_token


def authorization_header_valid(roles, username=None, password=None):
    header = {"authorization": "Bearer " + get_valid_token(roles, username, password)}
    return header
